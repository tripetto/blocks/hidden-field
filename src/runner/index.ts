/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    DateTime,
    HeadlessBlock,
    SHA2,
    assert,
    castToString,
    getAny,
    isObject,
    tripetto,
} from "@tripetto/runner";
import { IHiddenField } from "./interface";
import "./conditions/string";
import "./conditions/number";
import "./conditions/orientation";
import "./conditions/date";

@tripetto({
    type: "headless",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
})
export class HiddenField extends HeadlessBlock<IHiddenField> {
    readonly fieldSlot = assert(this.valueOf<string | number>("value"));

    do(): void {
        if (
            !this.fieldSlot.hasValue ||
            this.props.fieldType === "value" ||
            this.props.fieldType.indexOf("custom-variable:") === 0
        ) {
            switch (this.props.fieldType) {
                case "value":
                    // eslint-disable-next-line no-case-declarations
                    const value = this.parseVariables(
                        this.props.fieldValue || "",
                        "",
                        true
                    );

                    if (!this.fieldSlot.hasValue || value) {
                        this.fieldSlot.value = value;
                    }
                    break;
                case "uuid":
                    this.fieldSlot.value = SHA2.SHA2_256(
                        (() =>
                            ("" + 1e7 + -1e3 + -4e3 + -8e3 + -1e11).replace(
                                /1|0/g,
                                () => (0 | (Math.random() * 16)).toString(16)
                            ))()
                    );
                    break;
                case "timestamp":
                    this.fieldSlot.value = DateTime.UTC;
                    break;
                case "variable":
                    // eslint-disable-next-line no-case-declarations
                    let variable: string | undefined;

                    if (this.props.fieldVariable) {
                        try {
                            let w: Window | undefined = window;

                            while (!variable && w) {
                                variable = getAny(w, this.props.fieldVariable);

                                w =
                                    (w !== window.parent && window.parent) ||
                                    undefined;
                            }
                        } catch {
                            console.log(
                                `${PACKAGE_NAME}: Access to variable '${this.props.fieldVariable}' is prohibited!`
                            );
                        }
                    }

                    if (typeof variable === "string") {
                        this.fieldSlot.value = variable;
                    } else {
                        this.fieldSlot.clear();
                    }
                    break;
                case "language":
                    this.fieldSlot.value = navigator.language || "";
                    break;
                case "querystring":
                    this.fieldSlot.value =
                        (this.props.fieldQueryStringParameter
                            ? new URLSearchParams(
                                  document.location.search.substring(1)
                              ).get(this.props.fieldQueryStringParameter)
                            : document.location.search.substring(1)) || "";
                    break;
                case "cookie":
                    if (this.props.fieldCookie) {
                        const match = document.cookie.match(
                            RegExp(
                                `(?:^|;\\s*)${this.props.fieldCookie.replace(
                                    // eslint-disable-next-line no-useless-escape
                                    /([.*+?\^${}()|\[\]\/\\])/g,
                                    "\\$1"
                                )}=([^;]*)`
                            )
                        );

                        this.fieldSlot.value = (match && match[1]) || "";
                    } else {
                        this.fieldSlot.value = document.cookie || "";
                    }
                    break;
                case "localStorage":
                    // eslint-disable-next-line no-case-declarations
                    const localStorageValue =
                        this.props.fieldLocalStorage &&
                        localStorage.getItem(this.props.fieldLocalStorage);

                    this.fieldSlot.value =
                        typeof localStorageValue === "string"
                            ? localStorageValue
                            : "";
                    break;
                case "sessionStorage":
                    // eslint-disable-next-line no-case-declarations
                    const sessionStorageValue =
                        this.props.fieldSessionStorage &&
                        sessionStorage.getItem(this.props.fieldSessionStorage);

                    this.fieldSlot.value =
                        typeof sessionStorageValue === "string"
                            ? sessionStorageValue
                            : "";
                    break;
                case "user-agent":
                    this.fieldSlot.value = navigator.userAgent || "";
                    break;
                case "title":
                    this.fieldSlot.value = document.title || "";
                    break;
                case "url":
                    try {
                        this.fieldSlot.value =
                            document.URL || window.parent.location.href || "";
                    } catch {
                        this.fieldSlot.clear();

                        console.log(
                            `${PACKAGE_NAME}: Access to document URL is prohibited!`
                        );
                    }

                    break;
                case "referrer":
                    this.fieldSlot.value = document.referrer || "";
                    break;
                case "screenOrientation":
                    if (
                        window.screen &&
                        window.screen.orientation &&
                        window.screen.orientation.type
                    ) {
                        this.fieldSlot.value =
                            window.screen.orientation.type ===
                                "portrait-primary" ||
                            window.screen.orientation.type ===
                                "portrait-secondary"
                                ? "portrait"
                                : "landscape";
                    } else {
                        this.fieldSlot.value =
                            window.screen &&
                            window.screen.height > window.screen.width
                                ? "portrait"
                                : "landscape";
                    }

                    break;
                case "screenWidth":
                    this.fieldSlot.value =
                        (window.screen && window.screen.width) || 0;
                    break;
                case "screenHeight":
                    this.fieldSlot.value =
                        (window.screen && window.screen.height) || 0;
                    break;
                case "devicePixelRatio":
                    this.fieldSlot.value = window.devicePixelRatio || 1;
                    break;
                default:
                    if (
                        this.props.fieldType.indexOf("custom-variable:") === 0
                    ) {
                        let customVariables:
                            | {
                                  [variable: string]: string;
                              }
                            | undefined;

                        try {
                            let w: Window | undefined = window;

                            while (!customVariables && w) {
                                customVariables = getAny(
                                    w,
                                    "TRIPETTO_CUSTOM_VARIABLES"
                                );

                                w =
                                    (w !== window.parent && window.parent) ||
                                    undefined;
                            }
                        } catch {
                            customVariables = undefined;
                        }

                        if (isObject(customVariables)) {
                            this.fieldSlot.value = castToString(
                                getAny(
                                    customVariables,
                                    this.props.fieldType.replace(
                                        "custom-variable:",
                                        ""
                                    )
                                )
                            );
                        } else {
                            this.fieldSlot.clear();
                        }
                    }

                    break;
            }
        }
    }
}
