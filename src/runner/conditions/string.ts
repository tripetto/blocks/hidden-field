/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Str,
    condition,
    isVariable,
    tripetto,
} from "@tripetto/runner";
import { IHiddenFieldStringCondition } from "../interface";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: `${PACKAGE_NAME}:string`,
    alias: `${PACKAGE_NAME}-string`,
})
export class HiddenFieldStringCondition extends ConditionBlock<IHiddenFieldStringCondition> {
    private getMatchString() {
        if (isVariable(this.props.value)) {
            const variable = this.variableFor(this.props.value);

            return variable && variable.hasValue ? variable.string : "";
        }

        return this.parseVariables(this.props.value || "", "", true);
    }

    @condition
    isEqual(): boolean {
        const slot = this.valueOf<string>();

        if (slot) {
            const match = this.props.ignoreCase
                ? Str.lowercase(this.getMatchString())
                : this.getMatchString();
            const value = this.props.ignoreCase
                ? Str.lowercase(slot.string)
                : slot.string;

            switch (this.props.mode) {
                case "equals":
                    return value === match;
                case "not-equals":
                    return value !== match;
                case "contains":
                    return (match && value.indexOf(match) !== -1) || false;
                case "not-contains":
                    return (match && value.indexOf(match) === -1) || false;
                case "starts":
                    return (match && value.indexOf(match) === 0) || false;
                case "ends":
                    return (
                        (match &&
                            value.length >= match.length &&
                            value.lastIndexOf(match) ===
                                value.length - match.length) ||
                        false
                    );
                case "defined":
                    return value !== "";
                case "undefined":
                    return value === "";
                case "regex":
                    try {
                        const regex = this.props.regex || "";
                        const literalSignLeft = regex.indexOf("/");
                        const literalSignRight = regex.lastIndexOf("/");

                        return (
                            literalSignLeft === 0 &&
                            literalSignRight > literalSignLeft &&
                            ((v: string) => {
                                try {
                                    return (
                                        new RegExp(
                                            regex.substring(
                                                1,
                                                literalSignRight
                                            ),
                                            regex.substr(literalSignRight + 1)
                                        ).test(v) ===
                                        (this.props.invert ? false : true)
                                    );
                                } catch {
                                    return false;
                                }
                            })(slot.string)
                        );
                    } catch {
                        return false;
                    }
            }
        }

        return false;
    }
}
