/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Num,
    Slots,
    condition,
    isNumberFinite,
    isString,
    tripetto,
} from "@tripetto/runner";
import { IHiddenFieldNumberCondition } from "../interface";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: `${PACKAGE_NAME}:number`,
    alias: `${PACKAGE_NAME}-number`,
})
export class HiddenFieldNumberCondition extends ConditionBlock<IHiddenFieldNumberCondition> {
    private getValue(slot: Slots.Slot, value: number | string | undefined) {
        if (isString(value) && slot instanceof Slots.Number) {
            const variable = this.variableFor(value);

            return variable && variable.hasValue
                ? slot.toValue(variable.value)
                : undefined;
        }

        return isNumberFinite(value) ? value : undefined;
    }

    @condition
    compare(): boolean {
        const numberSlot = this.valueOf<number>();

        if (numberSlot) {
            const value = this.getValue(numberSlot.slot, this.props.value);

            switch (this.props.mode) {
                case "equal":
                    return (
                        (numberSlot.hasValue ? numberSlot.value : undefined) ===
                        value
                    );
                case "not-equal":
                    return (
                        (numberSlot.hasValue ? numberSlot.value : undefined) !==
                        value
                    );
                case "below":
                    return (
                        isNumberFinite(value) &&
                        numberSlot.hasValue &&
                        numberSlot.value < value
                    );
                case "above":
                    return (
                        isNumberFinite(value) &&
                        numberSlot.hasValue &&
                        numberSlot.value > value
                    );
                case "between":
                case "not-between":
                    // eslint-disable-next-line no-case-declarations
                    const to = this.getValue(numberSlot.slot, this.props.to);

                    return (
                        isNumberFinite(value) &&
                        isNumberFinite(to) &&
                        (numberSlot.hasValue &&
                            numberSlot.value >= Num.min(value, to) &&
                            numberSlot.value <= Num.max(value, to)) ===
                            (this.props.mode === "between")
                    );
            }
        }

        return false;
    }
}
