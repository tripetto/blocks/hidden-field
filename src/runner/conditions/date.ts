/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Num,
    Slots,
    condition,
    isNumberFinite,
    isString,
    tripetto,
} from "@tripetto/runner";
import { IHiddenFieldDateCondition } from "../interface";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: `${PACKAGE_NAME}:date`,
})
export class HiddenFieldDateCondition extends ConditionBlock<IHiddenFieldDateCondition> {
    private getValue(slot: Slots.Date, value: number | string | undefined) {
        if (isString(value) && slot instanceof Slots.Date) {
            const variable = this.variableFor(value);

            return variable && variable.hasValue
                ? slot.toValue(variable.value, "minutes")
                : undefined;
        }

        return isNumberFinite(value)
            ? slot.toValue(value, "minutes")
            : undefined;
    }

    @condition
    verify(): boolean {
        const dateSlot = this.valueOf<number, Slots.Date>();

        if (dateSlot) {
            const value = this.getValue(dateSlot.slot, this.props.value);
            const input = dateSlot.hasValue
                ? dateSlot.slot.toValue(dateSlot.value, "minutes")
                : undefined;

            switch (this.props.mode) {
                case "equal":
                    return input === value;
                case "not-equal":
                    return input !== value;
                case "before":
                    return (
                        isNumberFinite(value) &&
                        isNumberFinite(input) &&
                        input < value
                    );
                case "after":
                    return (
                        isNumberFinite(value) &&
                        isNumberFinite(input) &&
                        input > value
                    );
                case "between":
                case "not-between":
                    // eslint-disable-next-line no-case-declarations
                    const to = this.getValue(dateSlot.slot, this.props.to);

                    return (
                        isNumberFinite(value) &&
                        isNumberFinite(to) &&
                        (isNumberFinite(input) &&
                            input >= Num.min(value, to) &&
                            input <= Num.max(value, to)) ===
                            (this.props.mode === "between")
                    );
            }
        }

        return false;
    }
}
