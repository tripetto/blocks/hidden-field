export interface ICustomVariable {
    readonly name: string;
    readonly description: string;
}

export interface ICustomVariables {
    readonly name: string;
    readonly variables: ICustomVariable[];
}
