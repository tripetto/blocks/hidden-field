/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Forms,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    getHelpTopic,
    insertVariable,
    isString,
    map,
    pgettext,
    slots,
    tripetto,
} from "@tripetto/builder";
import { HiddenFieldStringCondition } from "./conditions/string";
import { HiddenFieldNumberCondition } from "./conditions/number";
import { HiddenFieldOrientationCondition } from "./conditions/orientation";
import { HiddenFieldDateCondition } from "./conditions/date";
import {
    IHiddenField,
    THiddenFieldDateConditionMode,
    THiddenFieldNumberConditionMode,
    THiddenFieldStringConditionMode,
} from "../runner/interface";
import { TFieldTypes } from "../runner/types";
import { getTypeDescription, getTypeName } from "./helpers";
import { ICustomVariables } from "./custom";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:hidden-field", "Hidden field");
    },
    kind: "headless",
})
export class HiddenField extends NodeBlock implements IHiddenField {
    static customVariables?: ICustomVariables;
    readonly allowMarkdown = false;

    @definition
    @affects("#name")
    @affects("#slots")
    fieldType: TFieldTypes | string = "value";

    @definition
    @affects("#name")
    fieldValue: string | undefined;

    @definition
    @affects("#name")
    fieldQueryStringParameter: string | undefined;

    @definition
    @affects("#name")
    fieldCookie: string | undefined;

    @definition
    @affects("#name")
    fieldLocalStorage: string | undefined;

    @definition
    @affects("#name")
    fieldSessionStorage: string | undefined;

    @definition
    @affects("#name")
    fieldVariable: string | undefined;

    fieldSlot!: Slots.String | Slots.Number | Slots.Date;

    @definition("boolean", "optional")
    @affects("#slots")
    required?: boolean;

    @definition("string", "optional")
    @affects("#slots")
    alias?: string;

    @definition("boolean", "optional")
    @affects("#slots")
    exportable?: boolean;

    get label() {
        let label = this.type.label + " / " + getTypeName(this.fieldType);

        if (this.fieldQueryStringParameter) {
            label += " / " + this.fieldQueryStringParameter;
        }

        if (this.fieldCookie) {
            label += " / " + this.fieldCookie;
        }

        if (this.fieldLocalStorage) {
            label += " / " + this.fieldLocalStorage;
        }

        if (this.fieldSessionStorage) {
            label += " / " + this.fieldSessionStorage;
        }

        if (this.fieldVariable) {
            label += " / " + this.fieldVariable;
        }

        return label;
    }

    @slots
    defineSlot(): void {
        const current = this.slots.select("value", "static");

        switch (this.fieldType) {
            case "devicePixelRatio":
            case "screenWidth":
            case "screenHeight":
                this.fieldSlot = this.slots.static({
                    type: Slots.Number,
                    reference: "value",
                    label: this.label,
                    required: this.required,
                    alias: this.alias,
                    exportable: this.exportable,
                    protected: true,
                    exchange: ["required", "alias", "exportable"],
                });
                break;
            case "timestamp":
                this.fieldSlot = this.slots.static({
                    type: Slots.Date,
                    reference: "value",
                    label: this.label,
                    required: this.required,
                    alias: this.alias,
                    exportable: this.exportable,
                    protected: true,
                    exchange: ["required", "alias", "exportable"],
                });
                break;
            default:
                this.fieldSlot = this.slots.static({
                    type: Slots.String,
                    reference: "value",
                    label: this.label,
                    required: this.required,
                    alias: this.alias,
                    exportable: this.exportable,
                    protected: current ? current.protected : true,
                    exchange: ["required", "alias", "exportable"],
                });
                break;
        }
    }

    @editor
    defineEditor(): void {
        const description = new Forms.Static(
            getTypeDescription(this.fieldType)
        ).markdown();
        const fnOptions = (...types: TFieldTypes[]) =>
            map(types, (type: TFieldTypes) => ({
                label: getTypeName(type),
                value: type,
            }));
        const helpTopic = getHelpTopic("block:hidden-field");

        this.editor.form({
            title: pgettext("block:hidden-field", "Explanation"),
            controls: [
                new Forms.Static(
                    pgettext(
                        "block:hidden-field",
                        "Generate a hidden field with a certain value and use that value in your form%1.",
                        helpTopic
                            ? ` ([${pgettext(
                                  "block:hidden-field",
                                  "learn more"
                              )}](${helpTopic}))`
                            : ""
                    )
                ).markdown(),
            ],
        });

        this.editor.name(
            false,
            false,
            pgettext("block:hidden-field", "Name"),
            false
        );

        this.editor.option({
            name: pgettext("block:hidden-field", "Type"),
            form: {
                title: pgettext("block:hidden-field", "Type of field"),
                controls: [
                    new Forms.Dropdown<TFieldTypes | string>(
                        [
                            ...(HiddenField.customVariables
                                ? [
                                      {
                                          optGroup:
                                              HiddenField.customVariables.name,
                                      },
                                      ...map(
                                          HiddenField.customVariables.variables,
                                          (variable) => ({
                                              label: variable.description,
                                              value: `custom-variable:${variable.name}`,
                                          })
                                      ),
                                  ]
                                : []),
                            {
                                optGroup: pgettext(
                                    "block:hidden-field",
                                    "Basic fields"
                                ),
                            },
                            ...fnOptions(
                                "value",
                                "timestamp",
                                "uuid",
                                "variable"
                            ),
                            {
                                optGroup: pgettext(
                                    "block:hidden-field",
                                    "Browser information"
                                ),
                            },
                            ...fnOptions(
                                "language",
                                "querystring",
                                "cookie",
                                "localStorage",
                                "sessionStorage",
                                "user-agent"
                            ),
                            {
                                optGroup: pgettext(
                                    "block:hidden-field",
                                    "Page information"
                                ),
                            },
                            ...fnOptions("title", "url", "referrer"),
                            {
                                optGroup: pgettext(
                                    "block:hidden-field",
                                    "Screen information"
                                ),
                            },
                            ...fnOptions(
                                "screenOrientation",
                                "screenWidth",
                                "screenHeight",
                                "devicePixelRatio"
                            ),
                        ],
                        Forms.Radiobutton.bind(this, "fieldType", "value")
                    ).on((type: Forms.Dropdown<TFieldTypes | string>) => {
                        description.label(getTypeDescription(this.fieldType));

                        fixedValueFeature.visible(type.value === "value");
                        fixedValueFeature.activated(type.value === "value");

                        querystringFeature.visible(
                            type.value === "querystring"
                        );
                        querystringFeature.activated(
                            type.value === "querystring" &&
                                isString(this.fieldQueryStringParameter)
                        );

                        cookieFeature.visible(type.value === "cookie");
                        cookieFeature.activated(
                            type.value === "cookie" &&
                                isString(this.fieldCookie)
                        );

                        localStorageFeature.visible(
                            type.value === "localStorage"
                        );
                        localStorageFeature.activated(
                            type.value === "localStorage"
                        );

                        sessionStorageFeature.visible(
                            type.value === "sessionStorage"
                        );
                        sessionStorageFeature.activated(
                            type.value === "sessionStorage"
                        );

                        variableFeature.visible(type.value === "variable");
                        variableFeature.activated(type.value === "variable");
                    }),
                ],
            },
            activated: true,
            locked: true,
        });

        this.editor.form({
            title: pgettext("block:hidden-field", "Function"),
            controls: [description],
        });

        const fnAddTextFeature = (
            name: string,
            title: string,
            property:
                | "fieldValue"
                | "fieldQueryStringParameter"
                | "fieldCookie"
                | "fieldLocalStorage"
                | "fieldSessionStorage"
                | "fieldVariable",
            activated: boolean,
            locked: boolean,
            visible: boolean = activated
        ) =>
            this.editor
                .option({
                    name,
                    form: {
                        title,
                        controls: [
                            new Forms.Text(
                                "singleline",
                                Forms.Text.bind(this, property, undefined)
                            ),
                        ],
                    },
                    activated,
                    locked,
                })
                .visible(visible);

        const fixedValueFeature = this.editor.option({
            name: pgettext("block:hidden-field", "Value"),
            form: {
                title: pgettext("block:hidden-field", "Fixed value"),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "fieldValue", undefined)
                    ).action("@", insertVariable(this, "validated")),
                ],
            },
            activated: this.fieldType === "value",
            locked: true,
        });

        const querystringFeature = fnAddTextFeature(
            pgettext("block:hidden-field", "Parameter"),
            pgettext("block:hidden-field", "Query string parameter"),
            "fieldQueryStringParameter",
            this.fieldType === "querystring" &&
                isString(this.fieldQueryStringParameter),
            false,
            this.fieldType === "querystring"
        );

        const cookieFeature = fnAddTextFeature(
            pgettext("block:hidden-field", "Cookie"),
            pgettext("block:hidden-field", "Cookie identifier"),
            "fieldCookie",
            this.fieldType === "cookie" && isString(this.fieldCookie),
            false,
            this.fieldType === "cookie"
        );

        const localStorageFeature = fnAddTextFeature(
            pgettext("block:hidden-field", "Item"),
            pgettext("block:hidden-field", "Name of item from local storage"),
            "fieldLocalStorage",
            this.fieldType === "localStorage",
            true
        );

        const sessionStorageFeature = fnAddTextFeature(
            pgettext("block:hidden-field", "Item"),
            pgettext("block:hidden-field", "Name of item from session storage"),
            "fieldSessionStorage",
            this.fieldType === "sessionStorage",
            true
        );

        const variableFeature = fnAddTextFeature(
            pgettext("block:hidden-field", "Variable"),
            pgettext("block:hidden-field", "Name of variable"),
            "fieldVariable",
            this.fieldType === "variable",
            true
        );

        this.editor.groups.options();
        this.editor.required(
            this,
            pgettext("block:hidden-field", "Hidden field value is required")
        );
        this.editor.visibility();
        this.editor.alias(this);
        this.editor.exportable(this);
    }

    @conditions
    defineCondition(): void {
        switch (this.fieldType) {
            case "screenOrientation":
                each(
                    [
                        {
                            mode: "landscape",
                            label: pgettext("block:hidden-field", "Landscape"),
                        },
                        {
                            mode: "portrait",
                            label: pgettext("block:hidden-field", "Portrait"),
                        },
                    ],
                    (condition: {
                        mode: "landscape" | "portrait";
                        label: string;
                    }) => {
                        this.conditions.template({
                            condition: HiddenFieldOrientationCondition,
                            label: condition.label,
                            props: {
                                slot: this.fieldSlot,
                                orientation: condition.mode,
                            },
                        });
                    }
                );
                break;
            case "screenWidth":
            case "screenHeight":
            case "devicePixelRatio":
                each(
                    [
                        {
                            mode: "equal",
                            label: pgettext(
                                "block:hidden-field",
                                "Number is equal to"
                            ),
                        },
                        {
                            mode: "not-equal",
                            label: pgettext(
                                "block:hidden-field",
                                "Number is not equal to"
                            ),
                        },
                        {
                            mode: "below",
                            label: pgettext(
                                "block:hidden-field",
                                "Number is lower than"
                            ),
                        },
                        {
                            mode: "above",
                            label: pgettext(
                                "block:hidden-field",
                                "Number is higher than"
                            ),
                        },
                        {
                            mode: "between",
                            label: pgettext(
                                "block:hidden-field",
                                "Number is between"
                            ),
                        },
                        {
                            mode: "not-between",
                            label: pgettext(
                                "block:hidden-field",
                                "Number is not between"
                            ),
                        },
                    ],
                    (condition: {
                        mode: THiddenFieldNumberConditionMode;
                        label: string;
                    }) => {
                        this.conditions.template({
                            condition: HiddenFieldNumberCondition,
                            label: condition.label,
                            props: {
                                slot: this.fieldSlot,
                                mode: condition.mode,
                            },
                        });
                    }
                );
                break;
            case "timestamp":
                each(
                    [
                        {
                            mode: "equal",
                            label: pgettext(
                                "block:hidden-field",
                                "Date is equal to"
                            ),
                        },
                        {
                            mode: "not-equal",
                            label: pgettext(
                                "block:hidden-field",
                                "Date is not equal to"
                            ),
                        },
                        {
                            mode: "before",
                            label: pgettext(
                                "block:hidden-field",
                                "Date is before"
                            ),
                        },
                        {
                            mode: "after",
                            label: pgettext(
                                "block:hidden-field",
                                "Date is after"
                            ),
                        },
                        {
                            mode: "between",
                            label: pgettext(
                                "block:hidden-field",
                                "Date is between"
                            ),
                        },
                        {
                            mode: "not-between",
                            label: pgettext(
                                "block:hidden-field",
                                "Date is not between"
                            ),
                        },
                    ],
                    (condition: {
                        mode: THiddenFieldDateConditionMode;
                        label: string;
                    }) => {
                        this.conditions.template({
                            condition: HiddenFieldDateCondition,
                            label: condition.label,
                            props: {
                                slot: this.fieldSlot,
                                mode: condition.mode,
                            },
                        });
                    }
                );
                break;
            default:
                each(
                    [
                        {
                            mode: "equals",
                            label: pgettext(
                                "block:hidden-field",
                                "Value matches"
                            ),
                        },
                        {
                            mode: "not-equals",
                            label: pgettext(
                                "block:hidden-field",
                                "Value does not match"
                            ),
                        },
                        {
                            mode: "contains",
                            label: pgettext(
                                "block:hidden-field",
                                "Value contains"
                            ),
                        },
                        {
                            mode: "not-contains",
                            label: pgettext(
                                "block:hidden-field",
                                "Value does not contain"
                            ),
                        },
                        {
                            mode: "starts",
                            label: pgettext(
                                "block:hidden-field",
                                "Value starts with"
                            ),
                        },
                        {
                            mode: "ends",
                            label: pgettext(
                                "block:hidden-field",
                                "Value ends with"
                            ),
                        },
                        {
                            mode: "defined",
                            label: pgettext(
                                "block:hidden-field",
                                "Value is not empty"
                            ),
                        },
                        {
                            mode: "undefined",
                            label: pgettext(
                                "block:hidden-field",
                                "Value is empty"
                            ),
                        },
                        {
                            mode: "regex",
                            label: pgettext(
                                "block:hidden-field",
                                "Regular expression"
                            ),
                        },
                    ],
                    (condition: {
                        mode: THiddenFieldStringConditionMode;
                        label: string;
                    }) => {
                        this.conditions.template({
                            condition: HiddenFieldStringCondition,
                            label: condition.label,
                            props: {
                                slot: this.fieldSlot,
                                mode: condition.mode,
                            },
                        });
                    }
                );
                break;
        }
    }
}
