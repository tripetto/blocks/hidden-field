/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    DateTime as DateTimeHelper,
    Forms,
    L10n,
    Slots,
    affects,
    definition,
    editor,
    isNumberFinite,
    isString,
    lookupVariable,
    pgettext,
    populateVariables,
    tripetto,
} from "@tripetto/builder";
import {
    IHiddenFieldDateCondition,
    THiddenFieldDateConditionMode,
} from "../../runner/interface";

/** Assets */
import ICON from "../../../assets/condition-date.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:date`,
    version: PACKAGE_VERSION,
    icon: ICON,
    autoOpen: true,
    get label() {
        return pgettext("block:hidden-field", "Verify date");
    },
})
export class HiddenFieldDateCondition
    extends ConditionBlock
    implements IHiddenFieldDateCondition
{
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    mode: THiddenFieldDateConditionMode = "equal";

    @definition
    @affects("#name")
    value?: number | string;

    @definition
    @affects("#name")
    to?: number | string;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        const slot = this.slot;

        if (slot instanceof Slots.Date) {
            const value = this.parse(slot, this.value);

            switch (this.mode) {
                case "between":
                    return `${value} ≤ @${slot.id} ≤ ${this.parse(
                        slot,
                        this.to
                    )}`;
                case "not-between":
                    return `@${slot.id} < ${value} ${pgettext(
                        "block:hidden-field",
                        "or"
                    )} @${slot.id} > ${this.parse(slot, this.to)}`;
                case "before":
                case "after":
                case "equal":
                case "not-equal":
                    return `@${slot.id} ${
                        this.mode === "after"
                            ? ">"
                            : this.mode === "before"
                            ? "<"
                            : this.mode === "not-equal"
                            ? "\u2260"
                            : "="
                    } ${value}`;
            }
        }

        return this.type.label;
    }

    get title() {
        return this.node?.label;
    }

    private static getToday(s: "begin" | "end"): number {
        return DateTimeHelper.UTCToday + (s === "end" ? 86400000 - 1 : 0);
    }

    private parse(
        slot: Slots.Date,
        value: number | string | undefined
    ): string {
        if (isNumberFinite(value)) {
            return L10n.locale.dateTimeShort(slot.toValue(value), true);
        } else if (
            isString(value) &&
            value &&
            lookupVariable(this, value)?.label
        ) {
            return `@${value}`;
        }

        return "\\_\\_";
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:hidden-field", "Compare mode"),
            controls: [
                new Forms.Radiobutton<THiddenFieldDateConditionMode>(
                    [
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Date is equal to"
                            ),
                            value: "equal",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Date is not equal to"
                            ),
                            value: "not-equal",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Date is before"
                            ),
                            value: "before",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Date is after"
                            ),
                            value: "after",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Date is between"
                            ),
                            value: "between",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Date is not between"
                            ),
                            value: "not-between",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on(
                    (
                        mode: Forms.Radiobutton<THiddenFieldDateConditionMode>
                    ) => {
                        to.visible(
                            mode.value === "between" ||
                                mode.value === "not-between"
                        );

                        switch (mode.value) {
                            case "equal":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If date equals"
                                );
                                break;
                            case "not-equal":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If date not equals"
                                );
                                break;
                            case "before":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If date is before"
                                );
                                break;
                            case "after":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If date is after"
                                );
                                break;
                            case "between":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If date is between"
                                );
                                break;
                            case "not-between":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If date is not between"
                                );
                                break;
                        }
                    }
                ),
            ],
        });

        const addCondition = (
            property: "value" | "to",
            title: string,
            visible: boolean
        ) => {
            const value = this[property];
            const variables = populateVariables(
                this,
                (slot) => slot instanceof Slots.Date,
                isString(value) ? value : undefined,
                true,
                this.slot?.id
            );
            const dateControl = new Forms.DateTime(
                isNumberFinite(value)
                    ? value
                    : HiddenFieldDateCondition.getToday(
                          property === "to" ? "end" : "begin"
                      )
            )
                .label(pgettext("block:hidden-field", "Use fixed date/time"))
                .features(
                    Forms.DateTimeFeatures.Date |
                        Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                )
                .years(
                    new Date().getFullYear() - 150,
                    new Date().getFullYear() + 50
                )
                .zone("UTC")
                .width("full")
                .required()
                .on((input) => {
                    if (input.isFormVisible && input.isObservable) {
                        this[property] = input.value;
                    }
                });

            const variableControl = new Forms.Dropdown(
                variables,
                isString(value) ? value : ""
            )
                .label(pgettext("block:hidden-field", "Use value of"))
                .width("full")
                .on((variable) => {
                    if (variable.isFormVisible && variable.isObservable) {
                        this[property] = variable.value || "";
                    }
                });

            return this.editor
                .form({
                    title,
                    controls: [
                        new Forms.Radiobutton<"variable" | "date">(
                            [
                                {
                                    label: pgettext(
                                        "block:hidden-field",
                                        "Fixed date/time"
                                    ),
                                    value: "date",
                                },
                                {
                                    label: pgettext(
                                        "block:hidden-field",
                                        "Value"
                                    ),
                                    value: "variable",
                                    disabled: variables.length === 0,
                                },
                            ],
                            isString(value) ? "variable" : "date"
                        ).on((type) => {
                            dateControl.visible(type.value === "date");
                            variableControl.visible(type.value === "variable");
                        }),
                        dateControl,
                        variableControl,
                    ],
                })
                .visible(visible);
        };

        const from = addCondition(
            "value",
            pgettext("block:hidden-field", "If date equals"),
            true
        );
        const to = addCondition(
            "to",
            pgettext("block:hidden-field", "And"),
            this.mode === "between" || this.mode === "not-between"
        );
    }
}
