/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Slots,
    Str,
    affects,
    definition,
    editor,
    insertVariable,
    isVariable,
    lookupVariable,
    makeMarkdownSafe,
    pgettext,
    populateVariables,
    tripetto,
} from "@tripetto/builder";
import {
    IHiddenFieldStringCondition,
    THiddenFieldStringConditionMode,
} from "../../runner/interface";

/** Assets */
import ICON from "../../../assets/condition-string.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:string`,
    alias: `${PACKAGE_NAME}-string`,
    version: PACKAGE_VERSION,
    icon: ICON,
    autoOpen: true,
    get label() {
        return pgettext("block:hidden-field", "Compare value");
    },
})
export class HiddenFieldStringCondition
    extends ConditionBlock
    implements IHiddenFieldStringCondition
{
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    mode: THiddenFieldStringConditionMode = "equals";

    @definition
    @affects("#name")
    value?: string;

    @definition
    ignoreCase?: boolean;

    @definition
    regex?: string;

    @definition
    invert?: boolean;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot instanceof Slots.String) {
            const wrap = (s: string) =>
                (s &&
                    (this.mode === "contains" ||
                        this.mode === "not-contains" ||
                        this.mode === "starts" ||
                        this.mode === "ends") &&
                    `_${s}_`) ||
                s;
            const match: string =
                (isVariable(this.value)
                    ? lookupVariable(this, this.value)?.label &&
                      `@${this.value}`
                    : this.value &&
                      wrap(
                          makeMarkdownSafe(Str.replace(this.value, "\n", "↵"))
                      )) || "\\_\\_";

            switch (this.mode) {
                case "equals":
                case "not-equals":
                    return `@${this.slot.id} ${
                        this.mode === "not-equals" ? "\u2260" : "="
                    } ${match}`;
                case "contains":
                case "not-contains":
                case "starts":
                case "ends":
                    return `@${this.slot.id} ${
                        this.mode === "not-contains"
                            ? pgettext("block:hidden-field", "does not contain")
                            : this.mode === "starts"
                            ? pgettext("block:hidden-field", "starts with")
                            : this.mode === "ends"
                            ? pgettext("block:hidden-field", "ends with")
                            : pgettext("block:hidden-field", "contains")
                    } ${match}`;
                case "defined":
                    return `@${this.slot.id} ${pgettext(
                        "block:hidden-field",
                        "not empty"
                    )}`;
                case "undefined":
                    return `@${this.slot.id} ${pgettext(
                        "block:hidden-field",
                        "empty"
                    )}`;
                case "regex":
                    return `@${this.slot.id} ${pgettext(
                        "block:hidden-field",
                        "regular expression"
                    )}`;
            }
        }

        return this.type.label;
    }

    get title() {
        return this.node?.label;
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:hidden-field", "Compare mode"),
            controls: [
                new Forms.Radiobutton<THiddenFieldStringConditionMode>(
                    [
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Value matches"
                            ),
                            value: "equals",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Value does not match"
                            ),
                            value: "not-equals",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Value contains"
                            ),
                            value: "contains",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Value does not contain"
                            ),
                            value: "not-contains",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Value starts with"
                            ),
                            value: "starts",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Value ends with"
                            ),
                            value: "ends",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Value is not empty"
                            ),
                            value: "defined",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Value is empty"
                            ),
                            value: "undefined",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Value satisfies regular expression"
                            ),
                            value: "regex",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equals")
                ).on(
                    (
                        mode: Forms.Radiobutton<THiddenFieldStringConditionMode>
                    ) => {
                        form.visible(
                            mode.value !== "defined" &&
                                mode.value !== "undefined" &&
                                mode.value !== "regex"
                        );
                        regex.visible(mode.value === "regex");

                        switch (mode.value) {
                            case "equals":
                                form.title = pgettext(
                                    "block:hidden-field",
                                    "If value matches"
                                );
                                break;
                            case "not-equals":
                                form.title = pgettext(
                                    "block:hidden-field",
                                    "If value does not match"
                                );
                                break;
                            case "contains":
                                form.title = pgettext(
                                    "block:hidden-field",
                                    "If value contains"
                                );
                                break;
                            case "not-contains":
                                form.title = pgettext(
                                    "block:hidden-field",
                                    "If value does not contain"
                                );
                                break;
                            case "starts":
                                form.title = pgettext(
                                    "block:hidden-field",
                                    "If value starts with"
                                );
                                break;
                            case "ends":
                                form.title = pgettext(
                                    "block:hidden-field",
                                    "If value ends with"
                                );
                                break;
                        }

                        if (textControl.isInteractable) {
                            textControl.focus();
                        }
                    }
                ),
            ],
        });

        const isVar = (this.value && isVariable(this.value)) || false;
        const variables = populateVariables(
            this,
            undefined,
            isVar ? this.value : undefined,
            false,
            this.slot?.id
        );
        const textControl = new Forms.Text(
            "multiline",
            !isVar ? this.value : ""
        )
            .label(pgettext("block:hidden-field", "Use fixed text"))
            .action("@", insertVariable(this, "exclude"))
            .autoFocus()
            .escape(this.editor.close)
            .on((input) => {
                if (input.isFormVisible && input.isVisible) {
                    this.value = input.value;
                }
            });
        const variableControl = new Forms.Dropdown(
            variables,
            isVar ? this.value : ""
        )
            .label(pgettext("block:hidden-field", "Use value of"))
            .width("full")
            .on((variable) => {
                if (variable.isFormVisible && variable.isObservable) {
                    this.value = variable.value || undefined;
                }
            });

        const form = this.editor
            .form({
                title: pgettext("block:hidden-field", "If value matches"),
                controls: [
                    new Forms.Radiobutton<"text" | "variable">(
                        [
                            {
                                label: pgettext("block:hidden-field", "Text"),
                                value: "text",
                            },
                            {
                                label: pgettext("block:hidden-field", "Value"),
                                value: "variable",
                                disabled: variables.length === 0,
                            },
                        ],
                        isVar ? "variable" : "text"
                    ).on((type) => {
                        textControl.visible(type.value === "text");
                        variableControl.visible(type.value === "variable");

                        if (textControl.isInteractable) {
                            textControl.focus();
                        }
                    }),
                    textControl,
                    variableControl,
                    new Forms.Checkbox(
                        pgettext("block:hidden-field", "Ignore case"),
                        Forms.Checkbox.bind(this, "ignoreCase", undefined, true)
                    ),
                ],
            })
            .visible(
                this.mode !== "defined" &&
                    this.mode !== "undefined" &&
                    this.mode !== "regex"
            );

        const regex = this.editor
            .form({
                title: pgettext(
                    "block:hidden-field",
                    "If value satisfies regular expression"
                ),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "regex", undefined, "")
                    )
                        .placeholder(
                            pgettext(
                                "block:hidden-field",
                                "Regex literal (for example /ab+c/)"
                            )
                        )
                        .autoFocus()
                        .autoValidate((regexControl) => {
                            if (!regexControl.value) {
                                return "unknown";
                            }

                            try {
                                const literalSignLeft =
                                    regexControl.value.indexOf("/");
                                const literalSignRight =
                                    regexControl.value.lastIndexOf("/");

                                return literalSignLeft === 0 &&
                                    literalSignRight > literalSignLeft &&
                                    ((r: string, n: number) => {
                                        try {
                                            return (
                                                new RegExp(
                                                    r.substring(1, n),
                                                    r.substr(n + 1)
                                                ) instanceof RegExp
                                            );
                                        } catch {
                                            return false;
                                        }
                                    })(regexControl.value, literalSignRight)
                                    ? "pass"
                                    : "fail";
                            } catch {
                                return "fail";
                            }
                        }),
                    new Forms.Checkbox(
                        pgettext(
                            "block:hidden-field",
                            "Invert regular expression"
                        ),
                        Forms.Checkbox.bind(this, "invert", undefined, false)
                    ),
                ],
            })
            .visible(this.mode === "regex");
    }
}
